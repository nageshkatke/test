<?php
$target_url = 'localhost/practice/curl_call_api.php';

$file = realpath('./sample.jpg');

$data = array(
    "UCC"           =>  "USER44",
    "Source"        =>  "MOB",
    "sessionId"     =>  "4c51154c-d2c5-46a8-958e-6975ef05e8c5",
    "CM"            =>  ""
);

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL,$target_url);
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_VERBOSE, 0);
curl_setopt($ch, CURLOPT_POST,1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
$result=curl_exec ($ch);
curl_close ($ch);
echo $result;
?>
