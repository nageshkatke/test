<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
// Complete the hourglassSum function below.
function hourglassSum($arr) {

	$max_sum = 7 * -9;
	

	for ($i = 0; $i <= 3 ; $i++)
	{
		$top = 0;
		$middle = 0;
		$bottom = 0;
		for ($j = 0; $j < 3 ; $j++)
		{
			//top
			$top = $top + $arr[$i][$j];
			echo "top = " . $top . " ";

			//middle
			$middle = $arr[$i+1][$j+1];
			echo "middle = " . $middle . " ";

			//bottom
			$bottom = $bottom + $arr[$i+2][$j];
			echo "bottom = " . $bottom . " ";
			
			$hourglass = $top + $middle + $bottom;
			echo "Hourglass = " . $hourglass . " ";

			if($hourglass > $max_sum)
				$max_sum = $hourglass;
			echo "Maxsum = " . $max_sum . "<br>";
		}
	}
	return $max_sum;
}

$arr = array();

$arr[0] = array(1, 1, 1, 0, 0, 0);
$arr[1] = array(0, 1, 0, 0, 0, 0);
$arr[2] = array(1, 1, 1, 0, 0, 0);
$arr[3] = array(0, 0, 2, 4, 4, 0);
$arr[4] = array(0, 0, 0, 2, 0, 0);
$arr[5] = array(0, 0, 1, 2, 4, 0);

$result = hourglassSum($arr);
echo $result;


?>